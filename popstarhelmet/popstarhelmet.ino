#include <Arduino.h>

// #define FASTLED_ESP8266_D1_PIN_ORDER
#include "FastLED.h"
FASTLED_USING_NAMESPACE

extern "C" {
	#include "user_interface.h"
}

#define DEBUG 1

#define LED_PIN 13
#define CLK_PIN 12

#define NUM_LEDS 12
#define NUM_ROWS 6
#define FRAMEDELAY 80
#define BRIGHTNESS 80

int patternPointer = 0;
int lastFrame = 0;

void blackout();
void framedelay( int frames );
void patternHourglass( int runTimes );
void patternSpectrum( int runTimes );
void patternFlash( int runTimes );
void patternCheckerboard( int runTimes );

void fill( CRGB color );
void oddEvenFill( CRGB oddColor, CRGB evenColor );

CRGB leds[NUM_LEDS];
int ledRef[NUM_LEDS];

int lowRes[8][3] = {
	{  0,  3,  4 },
	{  1,  2,  5 },
	{  7,  8, 11 },
	{  6,  9, 10 },
	{ 12, 15, 16 },
	{ 13, 14, 17 },
	{ 19, 20, 20 },
	{ 18, 21, 21 }
};

int tetrisShape[8] = {
	1 |   2 |   4 |  16,
	2 |   4 |   8 |  16,
	1 |   2 |   8 |  32,
	1 |   4 |   8 |  32,
	2 |   8 |  32 | 128,
	4 |   8 |  16 |  32,
	2 |   4 |   8 |  32,
	0
};

CHSV tetrisColor[8] = {
	CHSV( 180, 255, BRIGHTNESS),
	CHSV(   0, 255, BRIGHTNESS),
	CHSV(  60, 255, BRIGHTNESS),
	CHSV( 120, 255, BRIGHTNESS),
	CHSV( 240, 255, BRIGHTNESS),
	CHSV( 300, 255, BRIGHTNESS),
	CHSV( 180, 255, BRIGHTNESS),
	CHSV(   0,   0, BRIGHTNESS)
};


void setup() {
	FastLED.addLeds<WS2811, LED_PIN, BRG>(leds, NUM_LEDS);
	FastLED.setCorrection(TypicalLEDStrip);
    // FastLED.setBrightness(brightness);
    FastLED.setMaxPowerInVoltsAndMilliamps(5, 2000);

	// Swap the order of LEDs as the chain is
	// 0 1 - 3 2 - 4 5 - 7 6 - 8 9 - 11 10
	for (size_t i = 0; i < NUM_LEDS; i+=4) {
		ledRef[ i + 0 ] = i + 0;
		ledRef[ i + 1 ] = i + 1;
		ledRef[ i + 2 ] = i + 3;
		ledRef[ i + 3 ] = i + 2;
	}

	#ifdef DEBUG
		Serial.begin(115200);
	    delay(100);
	    Serial.setDebugOutput(true);
		Serial.println("Startup");

		Serial.println();
	    Serial.print( F("Heap: ") ); Serial.println(system_get_free_heap_size());
	    Serial.print( F("Boot Vers: ") ); Serial.println(system_get_boot_version());
	    Serial.print( F("CPU: ") ); Serial.println(system_get_cpu_freq());
	    Serial.print( F("SDK: ") ); Serial.println(system_get_sdk_version());
	    Serial.print( F("Chip ID: ") ); Serial.println(system_get_chip_id());
	    Serial.print( F("Flash ID: ") ); Serial.println(spi_flash_get_id());
	    Serial.print( F("Flash Size: ") ); Serial.println(ESP.getFlashChipRealSize());
	    Serial.print( F("Vcc: ") ); Serial.println(ESP.getVcc());
	    Serial.println();

	#endif

	// Hook the beat onto the universal timer
	lastFrame = millis();
	framedelay(1);
}

void loop() {
	#ifdef DEBUG
		Serial.print("patternPointer: ");
		Serial.println( patternPointer );
	#endif

	switch ( patternPointer ) {
		case 0: patternHourglass(8); break;
		// case 1: patternTetris(8); break;
		case 1: patternFlash(8); break;

		case 2: patternCheckerboard(8); break;
		case 3: patternSpectrum(16); break;
		default: patternPointer = -1; break;
	}

	patternPointer++;
}

/**
 * Set every pixel to black
 */
void blackout() {
	fill(CRGB::Black);
}

/*
 * We want to delay after painting a frame. But the delay needs to be
 * shortened by the amount of time it took to create and render the frame.
 *
 * This method will pause until the universal timer hits the next beat.
 *
 * @param frames     How many frames to delay
 */
void framedelay( int frames ) {

	// New lastFrame is the old lastFrame plus the number of frames we want to delay for
	lastFrame = lastFrame + ( frames * FRAMEDELAY );
	int now = millis();

	// Already past? Then return
	if( now > lastFrame ){
		return;
	}

	// Delay until we get to the (new) lastFrame
	delay( ( lastFrame - now ) );
}

/**
 * This pattern is the best three-LEDs-at-a-time ananlog I can get to
 * the monochrome hourglass shape in the movie.
 */
void patternHourglass( int runTimes ) {
	#ifdef DEBUG
		Serial.println("Pattern 1");
	#endif

	CRGB MONOCOLOR = CHSV( 0, 0, BRIGHTNESS);

	for( int iterator = 0; iterator < runTimes; iterator++ ){
		for ( int row = NUM_ROWS / 2; row >= 0; row-- ) {
			leds[ row * 2     ] = MONOCOLOR;
			leds[ row * 2 + 1 ] = MONOCOLOR;

			if( (NUM_ROWS - row) * 2      <  NUM_LEDS ) leds[ (NUM_ROWS - row) * 2     ] = MONOCOLOR;
			if( (NUM_ROWS - row) * 2 + 1  <  NUM_LEDS ) leds[ (NUM_ROWS - row) * 2 + 1 ] = MONOCOLOR;

			FastLED.show();
			framedelay( 1 );
		}
		blackout();
		framedelay( 1 );
	}
}

void patternSpectrum( int runTimes ) {
	#ifdef DEBUG
		Serial.println("Running Spectrum");
	#endif

	int hueIncrement = 32;

	for( int iterator = 0; iterator < runTimes; iterator++ ){
		int hue = 0;
		for ( int frame = 0; frame <= 8; frame++ ) {
			hue = (hue + hueIncrement) % 255;
			for ( int row = 0; row < NUM_ROWS; row++ ) {
				leds[ row * 2     ] = CHSV( hue + hueIncrement * row, 255, BRIGHTNESS );
				leds[ row * 2 + 1 ] = CHSV( hue + hueIncrement * row, 255, BRIGHTNESS );
			}

			FastLED.show();
			framedelay( 1 );
		}
	}
}

void patternFlash( int runTimes ) {
	#ifdef DEBUG
		Serial.println("Pattern 3");
	#endif

	int localBrightness = BRIGHTNESS * 2;
	if (localBrightness > 100) localBrightness = 100;

	int hue = 0;
	for ( int frame = 0; frame < runTimes; frame++ ) {
		if( hue > 255 ){
			hue = 0;
		}
		fill( CHSV( hue, 255, localBrightness ) );
		framedelay( 1 );

		blackout();
		framedelay( 7 );

		hue += 32;
	}
}

void patternCheckerboard( int runTimes ) {
	#ifdef DEBUG
		Serial.println("patternCheckerboard");
	#endif

	int hue = 0;
	for ( int frame = 0; frame < runTimes; frame++ ) {
		hue = (hue + 32) % 255;

		if ( frame % 2 == 0 ) {
			checkerFill( CHSV( hue, 255, BRIGHTNESS ), CRGB::Black );
		}
		else {
			checkerFill( CRGB::Black, CHSV( hue, 255, BRIGHTNESS ) );
		}
		framedelay( 8 );
	}
}

void patternTetris( int runTimes ) {
	#ifdef DEBUG
		Serial.println("patternTetris");
	#endif

	for (int i = 0; i < runTimes; i++) {
		blackout();
		lowResDraw( tetrisShape[i], tetrisColor[i] );
		FastLED.show();
		framedelay(8);
	}
}

void checkerFill( CRGB oddColor, CRGB evenColor ) {
	for( int row = 0; row < NUM_ROWS; row++ ){
		int checkerRow = row / 3;
		leds[ ledRef[ (row * 2) + 0 ] ] = ( checkerRow % 2 == 0 ) ? evenColor : oddColor;
		leds[ ledRef[ (row * 2) + 1 ] ] = ( checkerRow % 2 == 0 ) ? oddColor : evenColor;
	}
	FastLED.show();
}


void fill( CRGB color ) {
	for ( int i = 0; i < NUM_LEDS; i++ ) leds[i] = color;
	FastLED.show();
}

void oddEvenFill( CRGB oddColor, CRGB evenColor ) {
	for ( int i = 0; i < NUM_LEDS; i++ ) {
		leds[i] = ( i % 2 == 0 ) ? evenColor : oddColor;
	}
	FastLED.show();
}

void lowResDraw( int pattern, CHSV color ){
	// Powers of 2 from 0 to 7 = 0, 1, 2, 4, 8, 16, 32, 64 and 128 which
	// we'll use to send in a shape to this method.
	for (int p = 0; p <= 7; p++) {
		if( pattern & (1<<p) ){
			for (int i = 0; i < 3; i++) {
				if( lowRes[p][i] < NUM_LEDS ){
					leds[ lowRes[p][i] ] = color; //CHSV( color.h, 100, color.v );
				}
			}
		}
	}
}

void debugHSVColor( CHSV color ){
	#ifdef DEBUG
		Serial.print("Color: H:");
		Serial.print(color.h);
		Serial.print(" S:");
		Serial.print(color.s);
		Serial.print(" V:");
		Serial.print(color.v);
		Serial.println("");
	#endif
}
