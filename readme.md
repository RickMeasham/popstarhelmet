# Popstar: Never Stop Never Stopping

When it becomes clear that his solo album is a failure, a former boy band member does everything in his power to maintain his celebrity status.

http://www.imdb.com/title/tt3960412/

# Popstar Helmet

This code is part of a larger project to recreate the helmet Connor makes for his DJ and former band member, Owen.

## Timeline and Tasks

**UPDATE** Helment was a success and became known as LED Kelly. But it's too unweildy for regular use so my son and I are turning it into a tophat.

* ~~Source LEDs~~
* ~~Source Battery~~
* ~~Connect LEDs to Arduino~~
* ~~Write the hourglass and spectrum shower patterns~~
* ~~Split LED strips and wire them back together~~
* ~~Source helmet materials~~
* ~~Create prototype helmet (to make pattern)~~
* ~~Create helmet~~
* ~~Mount LEDs and Arduino~~

### New Tasks ###

* ~~Move to ESP8266 microprocessor~~
* ~~Find a top hat~~
* ~~Mount LEDs in top hat~~
* Connect LEDs to processor
* Add switch
* Consider an alternate battery arrangement better suited to the hat
* Add multi-panel capability rather than running all panels off a parallel control wire

### Extension Tasks ###
* Add a control interface using the Wifi capabilities of the ESP8266
* * Switch pattern manually
* * Fill with chosen color
* * Turn on and off
* Respond to microphone input and music beat automatically
